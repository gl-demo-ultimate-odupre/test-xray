module.exports = {
    collectCoverage: true,
    coverageDirectory: './coverage',
    reporters: ['default', 'jest-junit'],
    testMatch: ['**/?(*.)+(spec|test).[jt]s?(x)'],
    coverageReporters: ['html', 'text', 'text-summary', 'cobertura'],
  };

