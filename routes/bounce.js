// Generate a route to send back the file /../src/bounce.html safely, with imports and exports
// Add import to express and define router
const express = require('express');
const router = express.Router();
const path = require('path');

const bounceHtmlPath = path.join(__dirname, '..', 'src', 'bounce.html'); 

// Add a route to / to send the bouceHtml file
router.get('/', (req, res) => {
  res.sendFile(bounceHtmlPath);
});

// Export module
module.exports = router;
